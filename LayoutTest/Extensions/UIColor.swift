//
//  UIColor.swift
//  LayoutTest
//
//  Created by Agapov on 5/9/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit

extension UIColor {
    
    struct AppColors {
        static let background = UIColor(red: 0.15, green: 0.16, blue: 0.24, alpha: 1)
        static let gray = UIColor(red: 0.22, green: 0.23, blue: 0.31, alpha: 1)
        static let lightGray = UIColor(red: 0.27, green: 0.28, blue: 0.36, alpha: 1)
        static let blue = UIColor(red: 0.12, green: 0.56, blue: 0.95, alpha: 1)
        static let grayBlue = UIColor(red: 0.28, green: 0.29, blue: 0.4, alpha: 1)
    }
}
