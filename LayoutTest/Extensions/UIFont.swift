//
//  UIFont.swift
//  LayoutTest
//
//  Created by Agapov on 5/9/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit

extension UIFont {
    
    struct AppFonts {
        static func regular(size: CGFloat) -> UIFont {
            return UIFont(name: "Poppins-Regular", size: size)!
        }
        static func medium(size: CGFloat) -> UIFont {
            return UIFont(name: "Poppins-Medium", size: size)!
        }
        static func bold(size: CGFloat) -> UIFont {
            return UIFont(name: "Poppins-Bold", size: size)!
        }
        static func light(size: CGFloat) -> UIFont {
            return UIFont(name: "Poppins-Light", size: size)!
        }
    }
    
}
