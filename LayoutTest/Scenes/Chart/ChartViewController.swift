//
//  ChartViewController.swift
//  LayoutTest
//
//  Created by Agapov on 5/9/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit
import Charts

class ChartViewController: UIViewController {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var chartView: CustomChartView!
    @IBOutlet weak var pointView1: UIView!
    @IBOutlet weak var pointView2: UIView!
    @IBOutlet weak var chartTypeButton: UIButton!
    @IBOutlet weak var yearsChartCollectionView: UICollectionView!
    
    private let yearsToShowCount = 7
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
    }
    
    private func configureUI() {
        self.view.backgroundColor = UIColor.AppColors.background
        self.cardView.backgroundColor = UIColor.AppColors.gray
        self.pointView1.backgroundColor = UIColor.AppColors.blue
        self.pointView2.backgroundColor = UIColor.AppColors.blue
        self.chartTypeButton.backgroundColor = UIColor.AppColors.gray
        
        self.yearsChartCollectionView.backgroundColor = UIColor.AppColors.background
        self.yearsChartCollectionView.register(YearChartCollectionViewCell.self, forCellWithReuseIdentifier: YearChartCollectionViewCell.reuseId)
        self.yearsChartCollectionView.dataSource = self
        self.yearsChartCollectionView.delegate = self
    }
    
}

extension ChartViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return yearsToShowCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: YearChartCollectionViewCell.reuseId, for: indexPath) as! YearChartCollectionViewCell
        cell.setValue(percents: Float.random(in: 0...1))
        cell.yearLabel.text = "\(2012 + indexPath.row)"
        cell.chartView.scale(value: 2)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width - 40) / CGFloat(yearsToShowCount), height: collectionView.bounds.height)
    }
    
}
