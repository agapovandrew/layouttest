//
//  ChartView.swift
//  LayoutTest
//
//  Created by Agapov on 5/12/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Charts

class CustomChartView: PieChartView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.confugure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.confugure()
    }
    
    private func confugure() {
        let attributes = [
            NSAttributedString.Key.font: UIFont.AppFonts.light(size: 15),
            NSAttributedString.Key.foregroundColor: UIColor.white]
        self.centerAttributedText = NSAttributedString(string: "50%", attributes: attributes)
        self.drawEntryLabelsEnabled = false
        self.drawHoleEnabled = true
        self.holeColor = UIColor.AppColors.gray
        self.transparentCircleColor = .clear
        self.legend.enabled = false
        self.highlightPerTapEnabled = false
        self.holeRadiusPercent = 0.75
        self.legend.textColor = .clear
        
        let entry1 = PieChartDataEntry(value: 2)
        let entyre2 = PieChartDataEntry(value: 2)
        
        let set = PieChartDataSet(values: [entry1, entyre2], label: nil)
        set.drawValuesEnabled = false
        set.highlightEnabled = false
        set.selectionShift = 0
        set.sliceSpace = 2
        set.colors = [NSUIColor.init(cgColor: UIColor.AppColors.blue.cgColor),
                      NSUIColor.init(cgColor: UIColor.AppColors.lightGray.cgColor)]
        
        let data = PieChartData(dataSet: set)
        self.data = data
    }
    
    
}

