//
//  DashboardViewController.swift
//  LayoutTest
//
//  Created by Agapov on 5/9/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cardsCollectionView: UICollectionView!
    @IBOutlet weak var yearsChartsCollectionView: UICollectionView!
    
    private let daysToShow = [("January 01", "$2,029"), ("January 02", "$1,2"), ("January 03", "$1,1"), ("January 04", "$5,111"), ("January 05", "$2,1"), ("January 06", "$2,1")]
    
    private let yearsToShowCount = 7
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureUI()
    }
    
    private func configureUI() {
        self.view.backgroundColor = UIColor.AppColors.background
        
        self.cardsCollectionView.backgroundColor = UIColor.AppColors.background
        self.cardsCollectionView.register(UINib.init(nibName: CartCollectionViewCell.reuseId, bundle: nil), forCellWithReuseIdentifier: CartCollectionViewCell.reuseId)
        self.cardsCollectionView.dataSource = self
        self.cardsCollectionView.delegate = self
        
        self.yearsChartsCollectionView.backgroundColor = UIColor.AppColors.background
        self.yearsChartsCollectionView.register(YearChartCollectionViewCell.self, forCellWithReuseIdentifier: YearChartCollectionViewCell.reuseId)
        self.yearsChartsCollectionView.dataSource = self
        self.yearsChartsCollectionView.delegate = self
        
        self.tableView.backgroundColor = UIColor.AppColors.background
        self.tableView.register(UINib(nibName: DayTableViewCell.reuseId, bundle: nil), forCellReuseIdentifier: DayTableViewCell.reuseId)
        self.tableView.rowHeight = DayTableViewCell.cellHeight
        self.tableView.separatorStyle = .none
        self.tableView.dataSource = self
    }
    
}

extension DashboardViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.cardsCollectionView {
            return 2
        } else {
            return yearsToShowCount
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.cardsCollectionView {
            return collectionView.dequeueReusableCell(withReuseIdentifier: CartCollectionViewCell.reuseId, for: indexPath)
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: YearChartCollectionViewCell.reuseId, for: indexPath) as! YearChartCollectionViewCell
            cell.setValue(percents: Float.random(in: 0...1))
            cell.yearLabel.text = "\(2012 + indexPath.row)"
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.cardsCollectionView {
            return CGSize(width: self.view.bounds.width - 30, height: collectionView.bounds.height)
        } else {
            return CGSize(width: (self.view.bounds.width - 40) / CGFloat(yearsToShowCount), height: collectionView.bounds.height)
        }
    }
    
}

extension DashboardViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.daysToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DayTableViewCell.reuseId, for: indexPath) as! DayTableViewCell
        
        cell.topLineView.isHidden = indexPath.row == 0
        cell.bottomLineView.isHidden = (indexPath.row == self.daysToShow.count - 1)
        cell.dayNameLabel.text = self.daysToShow[indexPath.row].0
        cell.amountLabel.text = self.daysToShow[indexPath.row].1
        
        return cell
    }
    
}
