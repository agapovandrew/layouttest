//
//  CartCollectionViewCell.swift
//  LayoutTest
//
//  Created by Agapov on 5/9/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit
import Charts

class CartCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var chartView: CustomChartView!
    
    static let reuseId = String(describing: CartCollectionViewCell.self)

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.AppColors.background
        self.containerView.backgroundColor = UIColor.AppColors.gray
    }
    
}
