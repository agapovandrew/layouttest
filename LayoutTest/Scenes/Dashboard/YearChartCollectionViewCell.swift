//
//  YearChartCollectionViewCell.swift
//  LayoutTest
//
//  Created by Agapov on 5/10/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit

class YearChartCollectionViewCell: UICollectionViewCell {
    
    public let chartView = VerticalChartView()
    public let yearLabel =  UILabel()
    
    public static let reuseId = String(describing: YearChartCollectionViewCell.self)
    
    private var percentsValue: Float = 0
    private let yearLabelHeight: CGFloat = 20
    private let yearLabelFontSize: CGFloat = 10
    
    
    //MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.addConstraints()
        self.configureUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.addSubviews()
        self.addConstraints()
        self.configureUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.chartView.setValue(percents: self.percentsValue)
    }
    
    //MARK: - Private methods
    
    private func addSubviews() {
        self.addSubview(chartView)
        self.addSubview(yearLabel)
    }
    
    private func addConstraints() {
        self.yearLabel.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(yearLabelHeight)
        }
        
        self.chartView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalTo(self.yearLabel.snp.top)
        }
    }
    
    private func configureUI() {
        self.backgroundColor = UIColor.AppColors.background
        self.yearLabel.font = UIFont.AppFonts.medium(size: yearLabelFontSize)
        self.yearLabel.textColor = .white
        self.yearLabel.textAlignment = .center
    }
    
    //MARK: - Public methods
    
    public func setValue(percents: Float) {
        self.percentsValue = percents
    }
    
}

