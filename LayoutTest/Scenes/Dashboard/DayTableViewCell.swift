//
//  DayTableViewCell.swift
//  LayoutTest
//
//  Created by Agapov on 5/10/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit

class DayTableViewCell: UITableViewCell {
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dayNameLabel: UILabel!
    @IBOutlet weak var bottomLineView: UIView!
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var roundView: UIView!
    
    public static let reuseId = String(describing: DayTableViewCell.self)
    public static let cellHeight: CGFloat = 50

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.configureUI()
    }
    
    private func configureUI() {
        self.contentView.backgroundColor = UIColor.AppColors.background
        self.roundView.backgroundColor = UIColor.AppColors.blue
        self.topLineView.backgroundColor = UIColor.AppColors.grayBlue
        self.bottomLineView.backgroundColor = UIColor.AppColors.grayBlue
    }

    
}
