//
//  VerticalChartView.swift
//  LayoutTest
//
//  Created by Agapov on 5/10/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit
import SnapKit

class VerticalChartView: UIView {
    
    private let conteinerView = UIView()
    private let valueView = UIView()
    private var valueViewTopConstraint: Constraint?
    
    private let chartWidth: Float = 4
    private let chartCornerRadius: CGFloat = 2
    
    //MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.addConstraints()
        self.configureUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.addSubviews()
        self.addConstraints()
        self.configureUI()
    }
    
    //MARK: - Private methods
    
    private func addSubviews() {
        self.addSubview(conteinerView)
        self.conteinerView.addSubview(valueView)
    }
    
    private func addConstraints() {
        self.conteinerView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalTo(self.chartWidth)
            make.top.bottom.equalToSuperview()
        }
        
        self.valueView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            self.valueViewTopConstraint = make.top.equalTo(self.conteinerView.bounds.height).constraint
        }
    }
    
    private func configureUI() {
        self.backgroundColor = UIColor.AppColors.background
        self.conteinerView.backgroundColor = UIColor.AppColors.gray
        self.conteinerView.layer.cornerRadius = self.chartCornerRadius
        self.valueView.backgroundColor = UIColor.AppColors.blue
        self.valueView.layer.cornerRadius = self.chartCornerRadius
    }
    
    
    //MARK: - Public methods
    
    public func scale(value: Float) {
        self.conteinerView.snp.remakeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalTo(chartWidth * value)
            make.top.bottom.equalToSuperview()
        }
        self.conteinerView.layer.cornerRadius = chartCornerRadius * CGFloat(value)
        self.valueView.layer.cornerRadius = chartCornerRadius * CGFloat(value)
    }
    
    public func setValue(percents: Float) {
        self.valueViewTopConstraint?.update(offset: self.bounds.height * CGFloat(percents))
    }
}
