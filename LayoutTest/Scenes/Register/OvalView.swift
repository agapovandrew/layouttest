//
//  OvalView.swift
//  LayoutTest
//
//  Created by Agapov on 5/9/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit

class OvalView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        let ovalPath = UIBezierPath(ovalIn: rect)
        UIColor.AppColors.gray.setFill()
        ovalPath.fill()
    }

}
