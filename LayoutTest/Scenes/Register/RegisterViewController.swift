//
//  RegisterViewController.swift
//  LayoutTest
//
//  Created by Agapov on 5/9/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit
import SnapKit

class RegisterViewController: UIViewController {
    
    // MARK: - UI Elements
    
    private let registerLabel = UILabel()
    private let ovalView = OvalView()
    private let registerButton = UIButton()
    private let textFieldsStackView = UIStackView()
    private var textFields: [UITextField] = []
    
    // MARK: - Element's properties
    
    private let textfieldNames: [String] = ["Name", "Email", "Username", "Password", "Confirm Password"]
    private let textFieldHeight = 54
    private let textFieldsSpacing: CGFloat = 14
    private let textFieldsFontSize: CGFloat = 18
    private let textFieldsCornerRadius: CGFloat = 4
    
    private let ovalViewHeight = 16
    private let ovalViewWidth = 23
    private let ovalViewOffset = -8

    private let conntentLeftRightPadding = 36
    
    private let registerLabelHeight = 54
    private let registerLabelWidth = 209
    private let registerLabelFontSize: CGFloat = 36
    private let registerLabelLetterSpacing: CGFloat = 7
    private let registerLabelBottomOffset: CGFloat = -27
    private let registerLabelText = "Register"
    
    private let registerButtonHeight = 61
    private let registerButtonTopOffset = 25
    private let registerButtonFontSize: CGFloat = 20
    private let registerButtonCornerRadius: CGFloat = 4
    
    //MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.addConstraints()
        self.configureUI()
    }
    
    //MARK: - Private methods
    
    private func addSubviews() {
        self.view.addSubview(self.ovalView)
        self.view.addSubview(self.registerLabel)
        self.view.addSubview(self.textFieldsStackView)
        self.view.addSubview(self.registerButton)
        
        self.createTextFields(with: self.textfieldNames)
        self.textFields.forEach { textField in
            self.textFieldsStackView.addArrangedSubview(textField)
        }
    }
    
    private func addConstraints() {
        
        self.registerLabel.snp.makeConstraints { make in
            make.height.equalTo(registerLabelHeight)
            make.width.greaterThanOrEqualTo(registerLabelWidth)
            make.centerX.equalToSuperview()
            make.top.lessThanOrEqualTo(self.view.safeAreaLayoutGuide.snp.top).priorityMedium()
            make.bottom.equalTo(self.textFieldsStackView.snp.top).offset(registerLabelBottomOffset)
        }
        
        self.ovalView.snp.makeConstraints { make in
            make.centerY.equalTo(self.registerLabel.snp.centerY)
            make.leading.equalTo(self.registerLabel.snp.leading).offset(ovalViewOffset)
            make.width.equalTo(ovalViewWidth)
            make.height.equalTo(ovalViewHeight)
        }
        
        self.textFieldsStackView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(conntentLeftRightPadding)
            make.trailing.equalToSuperview().inset(conntentLeftRightPadding)
            make.centerY.equalToSuperview()
        }
        
        self.textFields.forEach{ textField in
            textField.snp.makeConstraints{ make in
                make.width.equalToSuperview()
                make.height.equalTo(textFieldHeight)
            }
        }
        
        self.registerButton.snp.makeConstraints { make in
            make.leading.trailing.equalTo(self.textFieldsStackView)
            make.height.equalTo(self.registerButtonHeight)
            make.top.equalTo(self.textFieldsStackView.snp.bottom).offset(self.registerButtonTopOffset)
            make.bottom.lessThanOrEqualTo(self.view.safeAreaLayoutGuide.snp.bottom).priorityMedium()
        }
    }
    
    private func configureUI() {
        self.view.backgroundColor = UIColor.AppColors.background
        
        self.textFieldsStackView.axis = .vertical
        self.textFieldsStackView.alignment = .center
        self.textFieldsStackView.spacing = self.textFieldsSpacing
        
        let attributes = [NSAttributedString.Key.kern: self.registerLabelLetterSpacing]
        self.registerLabel.attributedText = NSAttributedString(string: self.registerLabelText, attributes: attributes)
        self.registerLabel.font = UIFont.AppFonts.bold(size: registerLabelFontSize)
        self.registerLabel.textAlignment = .center
        self.registerLabel.textColor = UIColor.AppColors.blue
                
        self.registerButton.setTitle(self.registerLabelText.uppercased(), for: .normal)
        self.registerButton.backgroundColor = UIColor.AppColors.blue
        self.registerButton.titleLabel?.font = UIFont.AppFonts.regular(size: self.registerButtonFontSize)
        self.registerButton.layer.cornerRadius = self.registerButtonCornerRadius
    }
    
    private func createTextFields(with names: [String]) {
        self.textFields = names.map { name -> UITextField in
            
            let textfield = UITextField()
            textfield.textAlignment = .center
            textfield.textColor = .white
            textfield.backgroundColor = UIColor.AppColors.gray
            textfield.layer.cornerRadius = self.textFieldsCornerRadius
            textfield.font = UIFont.AppFonts.regular(size: textFieldsFontSize)
            
            let attributes = [
            NSAttributedString.Key.font: UIFont.AppFonts.regular(size: textFieldsFontSize),
            NSAttributedString.Key.foregroundColor: UIColor.white]
            textfield.attributedPlaceholder = NSAttributedString(string: name, attributes: attributes)
            
            return textfield
        }
    }
    
}
